package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Scanner;

import graphs.BidirectionalCouplingGraph;
import graphs.CallGraph;
import graphs.CallGraph2;
import graphs.CouplingGraph;
import graphs.StaticCallGraph;
import graphs.StaticCallGraph2;
import identificationModule.Cluster;
import identificationModule.CoupleOfClasses;
import identificationModule.HierarchicalClustering;
import metrics.CouplingMetric;
import processors.ASTProcessor;
import spoon.HierarchicalClusteringSpoon;
import spoon.SpoonCouplingGraph;
import spoon.SpoonCouplingMetric;
import spoon.SpoonParser;
import utility.Utility;

/**
 * 
 * @author ALLOUCH Yanis
 * @author KACI Ahmed
 */
public class CallGraphMain extends AbstractMain {

	@Override
	protected void menu() {
		StringBuilder builder = new StringBuilder();
		builder.append("1. Static call graph.");
		builder.append("\n2. Calculer la métrique de couplage entre deux classes A et B.");
		builder.append("\n3. Générez un graphe de couplage pondéré entre les classes de l'application.");
		builder.append("\n4. Générer le regroupement hiérarchique des classes.");
		builder.append("\n5. Générer les groupes de classes couplés (partitions).");
		builder.append("\n6. Calculer la métrique de couplage entre deux classes A et B en utilisant Spoon.");
		builder.append("\n7. Générez un graphe de couplage pondéré entre les classes de l'application. (avec Spoon).");
		builder.append("\n8. Générer le regroupement hiérarchique des classes (avec Spoon).");
		builder.append("\n9. Générer les groupes de classes couplés (partitions)s (avec Spoon).");
		builder.append("\n10. Dynamic call graph.");
		builder.append("\n11. Help menu.");
		builder.append("\n" + QUIT + ". To quit.");

		System.out.println(builder);
	}

	public static void main(String[] args) {
		CallGraphMain main = new CallGraphMain();
		BufferedReader inputReader;
		CallGraph callGraph = null;
		try {
			inputReader = new BufferedReader(new InputStreamReader(System.in));
			if (args.length < 1)
				setTestProjectPath(inputReader);
			else
				verifyTestProjectPath(inputReader, args[0]);
			String userInput = "";

			do {
				main.menu();
				userInput = inputReader.readLine();
				main.processUserInput(userInput, callGraph);
				Thread.sleep(3000);

			} while (!userInput.equals(QUIT));

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void processUserInput(String userInput, ASTProcessor processor) {
		CallGraph callGraph = (CallGraph) processor;
		CallGraph2 callGraph2 = (CallGraph2) processor;
		Scanner sc = new Scanner(System.in);
		CouplingMetric couplingMetric;
		CouplingGraph couplingGraph;
		BidirectionalCouplingGraph bidirectionalCouplingGraph;

		LinkedList<Cluster> clusters;
		LinkedList<CoupleOfClasses> couplesOfClasses;
		LinkedList<Cluster> partitions;
		SpoonParser spoonParser;
		SpoonCouplingGraph spoonCouplingGraph;
		HierarchicalClusteringSpoon hierarchicalClusteringSpoon;
		long start;
		long end;
		try {
			switch (userInput) {
			case "1": {
				callGraph = StaticCallGraph.createCallGraph(TEST_PROJECT_PATH);
				callGraph.log();
				break;
			}

			case "2": {
				callGraph2 = StaticCallGraph2.createCallGraph(TEST_PROJECT_PATH);
				System.out.println("Inserez le nom de la première classe svp");
				String classNameA = sc.next();
				System.out.println("Inserez le nom de la deuxième classe svp");
				String classNameB = sc.next();
				couplingMetric = new CouplingMetric(callGraph2);
				System.out.println("la métrique de couplage entre " + classNameA + " et " + classNameB + " = "
						+ (couplingMetric.getCouplingMetricBetweenAB(classNameA, classNameB)
								+ couplingMetric.getCouplingMetricBetweenAB(classNameB, classNameA)));
				break;
			}
			case "3": {

				start = System.currentTimeMillis();
				callGraph2 = StaticCallGraph2.createCallGraph(TEST_PROJECT_PATH);
				end = System.currentTimeMillis();
				System.out
						.println("Temps d'exécution de la generation du model par ASTParser : " + (end - start) + " Ms");
				start = System.currentTimeMillis();
				couplingMetric = new CouplingMetric(callGraph2);
				couplingGraph = new CouplingGraph(callGraph2, couplingMetric);
				couplingGraph.generateCouplingGraph();
				Utility.saveGraph(TEST_PROJECT_PATH + "UnidirectionalCouplingGraph.dot",
						Utility.getGraphAsDot(couplingGraph.getCouplings()));
				Utility.saveGraphAsPNG(TEST_PROJECT_PATH + "UnidirectionalCouplingGraph.png",
						Utility.getGraphAsDot(couplingGraph.getCouplings()));
				end = System.currentTimeMillis();
				System.out.println(
						"Temps d'exécution de génération du graphe de couplage unidirectionnel avec ASTParser : "
								+ (end - start) + " Ms");
				start = System.currentTimeMillis();
				bidirectionalCouplingGraph = new BidirectionalCouplingGraph(callGraph2, couplingMetric);
				bidirectionalCouplingGraph.generateBidirectionalCouplingGraph();
				Utility.saveGraph(TEST_PROJECT_PATH + "BidirectionalCouplingGraph.dot",
						Utility.getBidirectionalGraphAsDot(bidirectionalCouplingGraph.getBidirectionalCouplings()));
				Utility.saveGraphAsPNG(TEST_PROJECT_PATH + "BidirectionalCouplingGraph.png",
						Utility.getBidirectionalGraphAsDot(bidirectionalCouplingGraph.getBidirectionalCouplings()));
				end = System.currentTimeMillis();
				System.out
						.println("Temps d'exécution de génération du graphe de couplage bidirectionnel avec ASTParser : "
								+ (end - start) + " Ms");
				break;
			}

			case "4": {
				start = System.currentTimeMillis();
				callGraph2 = StaticCallGraph2.createCallGraph(TEST_PROJECT_PATH);
				end = System.currentTimeMillis();
				System.out
						.println("Temps d'exécution de la generation du model par ASTParser : " + (end - start) + " Ms");
				start = System.currentTimeMillis();
				couplingMetric = new CouplingMetric(callGraph2);
				bidirectionalCouplingGraph = new BidirectionalCouplingGraph(callGraph2, couplingMetric);
				bidirectionalCouplingGraph.generateBidirectionalCouplingGraph();
				end = System.currentTimeMillis();
				System.out
						.println("Temps d'exécution de génération du graphe de couplage bidirectionnel avec ASTParser : "
								+ (end - start) + " Ms");
				start = System.currentTimeMillis();
				HierarchicalClustering hierarchicalClustering = new HierarchicalClustering();
				clusters = hierarchicalClustering.createClustersInitilised(callGraph2);
				couplesOfClasses = hierarchicalClustering.createListOfClassesCouple(bidirectionalCouplingGraph);
				hierarchicalClustering.createHierarchicalClustering(clusters, couplesOfClasses);
				System.out.println(" \n \n ************** liste Final des clusters *************** \n \n");
				hierarchicalClustering.displayHierarchicalClustering(clusters);
				end = System.currentTimeMillis();
				System.out.println("Temps d'exécution de calcul des clusters avec ASTParser : " + (end - start) + " Ms");
				break;
			}
			case "5": {
				start = System.currentTimeMillis();
				callGraph2 = StaticCallGraph2.createCallGraph(TEST_PROJECT_PATH);
				end = System.currentTimeMillis();
				System.out
						.println("Temps d'exécution de la generation du model par ASTParser : " + (end - start) + " Ms");
				start = System.currentTimeMillis();
				couplingMetric = new CouplingMetric(callGraph2);
				couplingGraph = new CouplingGraph(callGraph2, couplingMetric);
				bidirectionalCouplingGraph = new BidirectionalCouplingGraph(callGraph2, couplingMetric);
				bidirectionalCouplingGraph.generateBidirectionalCouplingGraph();
				end = System.currentTimeMillis();
				System.out
						.println("Temps d'exécution de génération du graphe de couplage bidirectionnel avec ASTParser : "
								+ (end - start) + " Ms");
				start = System.currentTimeMillis();
				HierarchicalClustering hierarchicalClustering = new HierarchicalClustering();
				clusters = hierarchicalClustering.createClustersInitilised(callGraph2);
				couplesOfClasses = hierarchicalClustering.createListOfClassesCouple(bidirectionalCouplingGraph);
				partitions = hierarchicalClustering.createPartitions(clusters, couplesOfClasses);
				System.out.println(" \n \n ************** liste Final des partitions *************** \n \n");
				hierarchicalClustering.displayAllPartitions(partitions);
				end = System.currentTimeMillis();
				System.out
						.println("Temps d'exécution de calcul des partitions avec ASTParser : " + (end - start) + " Ms");
				break;
			}

			case "6": {
				start = System.currentTimeMillis();
				spoonParser = new SpoonParser(TEST_PROJECT_PATH);
				System.out.println("Inserez le nom de la première classe svp");
				String classNameA = sc.next();
				System.out.println("Inserez le nom de la deuxième classe svp");
				String classNameB = sc.next();
				SpoonCouplingMetric spoonCouplingMetric = new SpoonCouplingMetric(spoonParser.getModel());
				end = System.currentTimeMillis();
				System.out.println("Temps d'exécution de la generation du model par spoon : " + (end - start) + " Ms");
				start = System.currentTimeMillis();
				System.out.println("la métrique de couplage entre " + classNameA + " et " + classNameB + " = "
						+ (spoonCouplingMetric.getCouplingMetricBetweenAB(classNameA, classNameB)
								+ spoonCouplingMetric.getCouplingMetricBetweenAB(classNameB, classNameA)));
				end = System.currentTimeMillis();
				System.out.println("Temps d'exécution du calcul de la métrique avec spoon : " + (end - start) + " Ms");

				break;
			}

			case "7": {
				start = System.currentTimeMillis();
				spoonParser = new SpoonParser(TEST_PROJECT_PATH);
				spoonCouplingGraph = new SpoonCouplingGraph(spoonParser.getModel());
				end = System.currentTimeMillis();
				System.out.println("Temps d'exécution de la generation du model par spoon : " + (end - start) + " Ms");
				start = System.currentTimeMillis();
				spoonCouplingGraph.createUnidiractionalWeightedCouplingGraph();
				Utility.saveGraph(TEST_PROJECT_PATH + "UnidirectionalCouplingGraphSpoon.dot",
						Utility.getGraphAsDot(spoonCouplingGraph.getUnidirectionalWeightedCouplingGraph()));
				Utility.saveGraphAsPNG(TEST_PROJECT_PATH + "UnidirectionalCouplingGraphSpoon.png",
						Utility.getGraphAsDot(spoonCouplingGraph.getUnidirectionalWeightedCouplingGraph()));
				end = System.currentTimeMillis();
				System.out.println("Temps d'exécution de la generation du graph unidirectionnel avec spoon : "
						+ (end - start) + " Ms");

				start = System.currentTimeMillis();
				spoonCouplingGraph.createBidirectionalWeightedCouplingGraph();
				Utility.saveGraph(TEST_PROJECT_PATH + "BidirectionalGraphCouplingGraphSpoon.dot",
						Utility.getBidirectionalGraphAsDot(spoonCouplingGraph.getBidirectionalWeightedCouplingGraph()));
				Utility.saveGraphAsPNG(TEST_PROJECT_PATH + "BidirectionalGraphCouplingSpoon.png",
						Utility.getBidirectionalGraphAsDot(spoonCouplingGraph.getBidirectionalWeightedCouplingGraph()));
				end = System.currentTimeMillis();
				System.out.println("Temps d'exécution de la generation du graph bidirectionnel avec spoon : "
						+ (end - start) + " Ms");
				break;
			}

			case "8": {
				start = System.currentTimeMillis();
				spoonParser = new SpoonParser(TEST_PROJECT_PATH);
				spoonCouplingGraph = new SpoonCouplingGraph(spoonParser.getModel());
				end = System.currentTimeMillis();
				System.out.println("Temps d'exécution de la generation du model par spoon : " + (end - start) + " Ms");
				start = System.currentTimeMillis();
				spoonCouplingGraph.createBidirectionalWeightedCouplingGraph();
				end = System.currentTimeMillis();
				System.out.println("Temps d'exécution de la generation du graph bidirectionnel avec spoon : "
						+ (end - start) + " Ms");
				start = System.currentTimeMillis();
				hierarchicalClusteringSpoon = new HierarchicalClusteringSpoon(spoonParser.getModel());
				clusters = hierarchicalClusteringSpoon.createClustersInitialized();
				couplesOfClasses = hierarchicalClusteringSpoon.createListOfClassesCouple(spoonCouplingGraph);
				hierarchicalClusteringSpoon.createHierarchicalClustering(clusters, couplesOfClasses);
				System.out.println(" \n \n ************** liste Final des clusters *************** \n \n");
				hierarchicalClusteringSpoon.displayHierarchicalClustering(clusters);

				end = System.currentTimeMillis();
				System.out.println("Temps d'exécution de la generation du clustering hierarchique par spoon : "
						+ (end - start) + " Ms");
				break;
			}

			case "9": {
				start = System.currentTimeMillis();
				spoonParser = new SpoonParser(TEST_PROJECT_PATH);
				spoonCouplingGraph = new SpoonCouplingGraph(spoonParser.getModel());
				end = System.currentTimeMillis();
				System.out.println("Temps d'exécution de la generation du model par spoon : " + (end - start) + " Ms");
				start = System.currentTimeMillis();
				spoonCouplingGraph.createBidirectionalWeightedCouplingGraph();
				end = System.currentTimeMillis();
				System.out.println("Temps d'exécution de la generation du graph bidirectionnel avec spoon : "
						+ (end - start) + " Ms");
				start = System.currentTimeMillis();
				hierarchicalClusteringSpoon = new HierarchicalClusteringSpoon(spoonParser.getModel());
				clusters = hierarchicalClusteringSpoon.createClustersInitialized();
				couplesOfClasses = hierarchicalClusteringSpoon.createListOfClassesCouple(spoonCouplingGraph);
				partitions = hierarchicalClusteringSpoon.creatPartitions(clusters, couplesOfClasses);
				hierarchicalClusteringSpoon.displayAllPartitions(partitions);
				end = System.currentTimeMillis();
				System.out.println(
						"Temps d'exécution de la generation du partitionnement par spoon : " + (end - start) + " Ms");
				break;
			}

			case "10": {
				System.out.println("Not implemented yet.");
				break;
			}

			case "11": {
				return;
			}

			case QUIT:
				System.out.println("Bye...");
				return;

			default:
				System.err.println("Sorry, wrong input. Please try again.");
				return;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
